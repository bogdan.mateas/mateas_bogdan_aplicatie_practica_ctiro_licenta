# Use the official PHP image as base
FROM php:7.4-apache

# Install dependencies
RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    libxml2-dev \
    libzip-dev \
    unzip \
    wget \
    git \
    && docker-php-ext-install -j$(nproc) \
    pdo_mysql \
    bcmath \
    opcache \
    soap \
    gd \
    zip

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Download and install Magento
WORKDIR /var/www/html
RUN wget https://magento.mirror.hypernode.com/releases/magento2-latest.tar.gz && \
    tar -zxvf magento2-latest.tar.gz && \
    rm magento2-latest.tar.gz

# Set permissions
RUN chown -R www-data:www-data /var/www/html && \
    find /var/www/html -type d -exec chmod 755 {} \; && \
    find /var/www/html -type f -exec chmod 644 {} \;

# Expose port 80
EXPOSE 80

# Start Apache
CMD ["apache2-foreground"]
